package pe.com.entelgy;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Hello world!
 *
 */
public class App 
{
	
	final static Logger logger = LogManager.getLogger(App.class);
	
    public static void main( String[] args )
    {
    	logger.info("Hello World!");
    }
    
    public int sum (int a, int b) {
    	int c = a + b;
    	logger.info("Primer valor " + a);
    	logger.info("Segundo valor " + b);
    	logger.info("Resultado es: ", c);
		return c;
    	
    }
}
